package com.utz.noah.weatherapp.main;

import com.utz.noah.weatherapp.BasePresenter;
import com.utz.noah.weatherapp.BaseView;
import com.utz.noah.weatherapp.data.model.Weather;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class MainContract {

    public interface View extends BaseView<Presenter> {

        void setWeatherData(Weather weatherData);
        void showErrorData();

    }

    public interface Presenter extends BasePresenter {

        void fetchData(String query);

    }

}
