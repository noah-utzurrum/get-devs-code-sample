package com.utz.noah.weatherapp.data.model;

import java.util.Locale;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class MainData {

    String temp;

    public String getTempDisplay() {
        return String.format(Locale.getDefault(), "%s \u00B0C", temp);
    }

}
