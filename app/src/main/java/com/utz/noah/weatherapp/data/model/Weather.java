package com.utz.noah.weatherapp.data.model;

import com.utz.noah.weatherapp.utility.StringUtils;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class Weather {

    String name;
    WeatherData[] weather;
    String dt;
    MainData main;

    public String getName() {
        return name;
    }

    public WeatherData getWeather() {
        if (weather.length > 0) {
            return weather[0];
        }
        return null;
    }

    public String getDateTime() {
        return StringUtils.toDateDisplay(dt);
    }

    public MainData getMain() {
        return main;
    }
}
