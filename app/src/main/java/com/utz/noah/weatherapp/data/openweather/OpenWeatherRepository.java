package com.utz.noah.weatherapp.data.openweather;

import com.google.gson.Gson;
import com.utz.noah.weatherapp.data.IWeatherRepository;
import com.utz.noah.weatherapp.data.model.Weather;
import com.utz.noah.weatherapp.utility.NetworkUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class OpenWeatherRepository implements IWeatherRepository {

    private static String REQUEST = "http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric&appId=2cbce8945a0a6efd8371602eee8f0387";

    private static OpenWeatherRepository mRepository;

    public static OpenWeatherRepository getInstance() {
        if (mRepository == null) {
            mRepository = new OpenWeatherRepository();
        }
        return mRepository;
    }

    private OpenWeatherRepository() {
    }

    private Weather parseData(String data) {
        Gson gson = new Gson();
        return gson.fromJson(data, Weather.class);
    }

    @Override
    public Weather getData(String query) {
        try {
            query = URLEncoder.encode(query, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String request = String.format(Locale.getDefault(), REQUEST, query);
        String data = NetworkUtils.getResponse(request);
        Weather weatherData = parseData(data);
        return weatherData;
    }
}
