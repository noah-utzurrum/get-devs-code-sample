package com.utz.noah.weatherapp.data.model;

import com.utz.noah.weatherapp.utility.StringUtils;

import java.util.Locale;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class WeatherData {

    private static final String ICON_URL = "http://openweathermap.org/img/w/%s.png";
    String description;
    String icon;

    public String getDescription() {
        return StringUtils.capitalizeSentence(description);
    }

    public String getIconLink() {
        return String.format(Locale.getDefault(), ICON_URL, icon);
    }

}
