package com.utz.noah.weatherapp.utility;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class NetworkUtils {

    public static String getResponse(String strUrl) {
        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url
                    .openConnection();

            InputStream in = urlConnection.getInputStream();

            InputStreamReader isw = new InputStreamReader(in);

            StringBuilder stringBuilder = new StringBuilder();
            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                stringBuilder.append(current);
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
}
