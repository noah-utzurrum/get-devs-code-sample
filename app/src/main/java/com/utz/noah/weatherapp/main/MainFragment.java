package com.utz.noah.weatherapp.main;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.utz.noah.weatherapp.R;
import com.utz.noah.weatherapp.data.model.Weather;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements MainContract.View {

    private MainContract.Presenter mPresenter;
    
    private TextView mCity;
    private TextView mDateTime;
    private TextView mWeather;
    private ImageView mIcon;
    private TextView mTemperature;
    private View mCardView;

    public MainFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mCity = view.findViewById(R.id.city);
        mDateTime = view.findViewById(R.id.date_time);
        mWeather = view.findViewById(R.id.weather);
        mIcon = view.findViewById(R.id.icon);
        mTemperature = view.findViewById(R.id.temperature);
        mCardView = view.findViewById(R.id.cardViewWeather);

        final EditText query = view.findViewById(R.id.query);
        view.findViewById(R.id.buttonGo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.fetchData(query.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setWeatherData(Weather weatherData) {
        mCity.setText(weatherData.getName());
        mDateTime.setText(weatherData.getDateTime());
        mWeather.setText(weatherData.getWeather().getDescription());
        Glide.with(this)
                .load(weatherData.getWeather().getIconLink())
                .into(mIcon);
        mTemperature.setText(weatherData.getMain().getTempDisplay());
        mCardView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorData() {
        Toast.makeText(getContext(), "Failed loading data", Toast.LENGTH_SHORT).show();
        mCardView.setVisibility(View.GONE);
    }
}
