package com.utz.noah.weatherapp.data;

import com.utz.noah.weatherapp.data.model.Weather;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public interface IWeatherRepository {

    interface GetDataCallback {
        void onDataLoaded(Weather weatherData);
    }

    Weather getData(String query);

}
