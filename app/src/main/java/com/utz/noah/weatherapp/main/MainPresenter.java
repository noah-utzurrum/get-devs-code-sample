package com.utz.noah.weatherapp.main;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.utz.noah.weatherapp.data.IWeatherRepository;
import com.utz.noah.weatherapp.data.model.Weather;
import com.utz.noah.weatherapp.data.WeatherLoader;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class MainPresenter implements MainContract.Presenter,
        LoaderManager.LoaderCallbacks<Weather> {

    private static final int QUERY_WEATHER_DATA = 1000;
    private static final String PARAM_QUERY = "query";

    private final LoaderManager mLoaderManager;
    private final IWeatherRepository mRepository;
    private final MainContract.View mView;
    private final Context mContext;

    public MainPresenter(LoaderManager loaderManager, IWeatherRepository repository,
                         MainContract.View view, Context context) {
        mLoaderManager = loaderManager;
        mRepository = repository;
        mView = view;
        mView.setPresenter(this);
        mContext = context;
    }

    @Override
    public void fetchData(String query) {
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_QUERY, query);
        mLoaderManager.restartLoader(QUERY_WEATHER_DATA, bundle, this);
    }

    @Override
    public Loader<Weather> onCreateLoader(int id, Bundle args) {
        String query = args.getString(PARAM_QUERY);
        return new WeatherLoader(mContext, mRepository, query);
    }

    @Override
    public void onLoadFinished(Loader<Weather> loader, Weather weatherData) {
        if (weatherData != null) {
            mView.setWeatherData(weatherData);
        } else {
            mView.showErrorData();
        }
    }

    @Override
    public void onLoaderReset(Loader<Weather> loader) {
        loader = null;
    }
}
