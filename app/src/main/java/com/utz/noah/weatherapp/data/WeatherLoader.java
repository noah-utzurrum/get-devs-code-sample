package com.utz.noah.weatherapp.data;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.utz.noah.weatherapp.data.model.Weather;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class WeatherLoader extends AsyncTaskLoader<Weather> {

//    private static WeatherLoader mInstance;

    private final IWeatherRepository mRepository;

    private String mQuery;

//    public static WeatherLoader getInstance(Context context, IWeatherRepository repository) {
//        if (mInstance == null) {
//            mInstance = new WeatherLoader(context, repository);
//        }
//        return mInstance;
//    }

//    private WeatherLoader(Context context, IWeatherRepository repository) {
//        super(context);
//        mRepository = repository;
//    }

    public WeatherLoader(Context context, IWeatherRepository repository, String query) {
        super(context);
        mRepository = repository;
        mQuery = query;
    }

    @Override
    public Weather loadInBackground() {
        return mRepository.getData(mQuery);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public void deliverResult(Weather data) {
        super.deliverResult(data);
    }

//    public void setQuery(String query) {
//        mQuery = query;
//    }
}
