package com.utz.noah.weatherapp.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.utz.noah.weatherapp.R;
import com.utz.noah.weatherapp.data.IWeatherRepository;
import com.utz.noah.weatherapp.data.WeatherLoader;
import com.utz.noah.weatherapp.data.openweather.OpenWeatherRepository;

public class MainActivity extends AppCompatActivity {

    private MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MainFragment fragment =
                (MainFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) {
            fragment = new MainFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
        }

        IWeatherRepository repository = OpenWeatherRepository.getInstance();
        mPresenter = new MainPresenter(getSupportLoaderManager(), repository, fragment, this);

    }

}
