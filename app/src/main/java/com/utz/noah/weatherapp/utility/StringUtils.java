package com.utz.noah.weatherapp.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by noahutzurrum on 04/08/2017.
 */

public class StringUtils {

    public static String capitalizeSentence(String sentence) {
        return sentence.substring(0,1).toUpperCase() + sentence.substring(1);
    }

    public static String toDateDisplay(String dateTime) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
            calendar.setTimeInMillis(Long.parseLong(dateTime));
            DateFormat outputFmt = new SimpleDateFormat("EEEE h:mm a", Locale.getDefault());
            return outputFmt.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
